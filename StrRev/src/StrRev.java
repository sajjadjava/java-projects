import java.util.Scanner;
public class StrRev {
    public static void main(String[] args)
    {
        Scanner in=new Scanner(System.in);
        String str=in.nextLine();
        int tol=str.length();
        for (int i=tol-1;i>=0;i--)
        {
            System.out.print(str.charAt(i));
        }
    }
}
