import java.util.Scanner;
public class Factoriel {
    public static void main(String[] args)
    {
        Scanner in=new Scanner(System.in);
        int n=in.nextInt(),factoriel=1;
        for (int i=1;i<=n;i++)
        {
            factoriel *= i;
        }
        System.out.print(factoriel);
    }
}
