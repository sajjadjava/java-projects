import java.util.Scanner;
public class FiboBin {
    public static void main(String[] args)
    {
        Scanner in=new Scanner(System.in);
        int vorodi=in.nextInt();
        int i=1;
        while (calculate_fibo_i(i) <= vorodi)
        {
            if ((calculate_fibo_i(i) + calculate_bin_x(calculate_fibo_i(i))) == vorodi )
                System.out.print(" the number is FiboBinary");
            i++;
        }
    }
    public static int calculate_bin_x(int number)
    {
        int binary=0,number2=number,binary_make=1,tedade_yek_ha=0;
        while (number2 != 0)
        {
            binary = binary + (number2%2)*binary_make;
            binary_make *= 10;
            number2 = number2/10;
        }
        while (binary != 0)
        {
            if (binary %10 == 1)
            {
                tedade_yek_ha++;
            }
            binary /= 10;
        }
        return tedade_yek_ha;
    }
    public static int calculate_fibo_i(int adad)
    {
        int first=0,second=1,negahdar=0;
        if (adad==1) {
            return 0;
        }
        else if (adad==2)
        {
            return 1;
        }
        else
        {
            for (int i=0;i<adad-2;i++)
            {
                negahdar=second;
                second=first+second;
                first=negahdar;
            }
            return second;
        }
    }
}
