import java.util.Scanner;
public class MatrixProduct {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("arze matrise aval ra vared konid: ");
        int arze_first = in.nextInt();
        System.out.println("tole matrise aval va arze matrise dovom ke barabar hastand ra vared konid: ");
        int tole_first_va_arze_second = in.nextInt();
        System.out.println("tole matrise dovom ra vared konid");
        int tole_second = in.nextInt();
        int[][] first = new int[arze_first][tole_first_va_arze_second];
        int[][] second = new int[tole_first_va_arze_second][tole_second];
        int[][] javab = new int[arze_first][tole_second];
        System.out.println("matrise aval ra vared konid bad az har satr enter ra bezanid");
        for (int i = 0; i < arze_first; i++) {
            for (int j = 0; j < tole_first_va_arze_second; j++) {
                first[i][j] = in.nextInt();
            }
            System.out.println("");
        }
        System.out.println("matrise dovom ra vared konid bad az har satr enter ra bezanid");
        for (int i = 0; i < tole_first_va_arze_second; i++) {
            for (int j = 0; j < tole_second; j++) {
                second[i][j] = in.nextInt();
            }
            System.out.println("");
        }
        zarbe_matrisha(first,second,javab);
        System.out.println("javab is");
        for (int i=0;i<javab.length;i++)
        {
            for (int j=0;j<(javab[0].length);j++)
            {
                System.out.print(javab[i][j] +" ");
            }
            System.out.println("");
        }
    }
    public static void zarbe_matrisha(int[][] first,int[][] second,int[][] javab)
    {
        for (int i=0;i<first.length;i++)
        {
            for (int j=0;j<(second[0].length);j++)
            {
                javab[i][j]=0;
                for (int z=0;z<second.length;z++)
                {
                    javab[i][j] += (first[i][z]*second[z][j]);
                }
            }
        }
    }
}
