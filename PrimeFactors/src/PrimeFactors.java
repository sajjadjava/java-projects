import java.util.Scanner;
public class PrimeFactors {
    public static void main(String[] args)
    {
        Scanner in=new Scanner(System.in);
        int vorodi=in.nextInt();
        System.out.println("");
        avamele_aval(vorodi);
    }
    public static void avamele_aval(int vorodi)
    {
        if (vorodi==1 || vorodi==2)
            return;
        else {
            for (int i = 2; i <= vorodi; i++) {
                if (aval_bodan(i) && vorodi%i==0) {
                    System.out.print(i + " ");
                }
            }
        }
    }
    public static boolean aval_bodan(int number)
    {
        if (number<=1)
            return false;
        else if (number==2)
            return true;
        else
        {
            for (int i=2;i<number;i++)
            {
                if (number%i==0)
                    return false;
            }
            return true;
        }
    }
}
