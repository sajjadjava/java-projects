import java.util.Scanner;
import java.lang.Math;
public class Guess {
    public static void main(String[] args)
    {
        Scanner in=new Scanner(System.in);
        int target=(int)Math.random()*10000;
        int guess=in.nextInt();
        while (guess!=target)
        {
            if (guess < target)
                System.out.println("Your guess is too low");
            if (guess > target)
                System.out.print("Your guess is too high");
            guess=in.nextInt();
        }
    }
}
